<?php

namespace Neneff\Sql;



class Filter
{
    const S_AND = 'AND';
    const S_OR  = 'OR';

    /** @var string */
    public $condition;

    /** @var array */
    public $values;

    /**
     * Filter constructor.
     * @param string $condition
     * @param array  $values
     */
    public function __construct($condition = 'TRUE', array $values = [])
    {
        $this->condition = $condition;
        $this->values    = $values;
    }

    /**
     * @param String   $separator (AND |OR)
     * @param Filter[] ...$filters
     * @return Filter
     */
    public function concat($separator, Filter ...$filters)
    {
        $conditions = [$this->condition];
        $values     = $this->values;

        foreach($filters as $filter)
        {
            $conditions[] = $filter->condition;
            if(count($filter->values) > 0)
            {
                array_push($values, ...$filter->values);
            }
        }

        $condition = join(" $separator ", $conditions);

        // -- Parenthesis if OR
        if((count($conditions) > 0) && ($separator == self::S_OR))
        {
            $condition = "($condition)";
        }
        return new Filter($condition, $values);
    }

    /**
     * @return array['condition' => ..., 'values' => [...]]
     */
    public function toArray()
    {
        return  [
            'condition' => $this->condition,
            'values'    => $this->values
        ];
    }

}


